#include "Game.hpp"
#include "EventManager.hpp"
#include <SFML/Graphics.hpp>
#include <stdexcept>
#include <cassert>
#include <cstdio>
#include <cstdlib>

Game* Game::_instance = nullptr;

Game* Game::instance() {
    if (_instance == nullptr)
        _instance = new Game();
    return _instance;
}


Game::Game(): m_input_manager(), m_sound_manager(), m_music_manager(),
 active_state(nullptr), prev_state(nullptr), m_animation_manager(),
 m_window_size(GameConsts::WIDTH, GameConsts::HEIGHT)
  { 
    m_input_manager.register_input("W", "V+", sf::Keyboard::W);
    m_input_manager.register_input("S", "V-", sf::Keyboard::S);
    m_input_manager.register_input("A", "H-", sf::Keyboard::A);
    m_input_manager.register_input("D", "H+", sf::Keyboard::D);
    m_input_manager.register_input("Space", "Fire", sf::Keyboard::Space);
    m_input_manager.register_input("Enter", "Enter", sf::Keyboard::Enter);
    m_input_manager.register_input("Escape", "Escape", sf::Keyboard::Escape);

    auto ext = ".ogg";
    for (auto& _name: { "block-broken", "click", "damage",
        "enemy-boom", "fire", "pause", "player-boom", "stage-start", 
        "unpause" })
        m_sound_manager.loadFromFile(
            std::string(_name), 
            std::string("../res/Sounds/").append(_name).append(ext)
        );

    m_music_manager.loadFromFile(
        std::string("background"), 
        std::string("../res/Music/").append("background").append(ext)
    );
    m_animation_manager.loadTextureFromFile(std::string("../res/Textures/sprite.bmp"));
    // basically, really cool idea is to show this state when game initializes
    // static duration. needs improving for potentially consuming staff
    active_state = new GameLoadingState(1.f);
    
}

void Game::run() {
    sf::Clock clock;
    sf::Time time;

    sf::Time currTime = clock.getElapsedTime();
    sf::Time accumulator = sf::seconds(0.0);
    sf::Event event;
    sf::RenderWindow window(sf::VideoMode(m_window_size.x, m_window_size.y), GameConsts::TITLE);
    while (window.isOpen()) {
        if (window.hasFocus()) {
            if (window.pollEvent(event)) {
                if (event.type == sf::Event::Closed)
                        std::exit(0);
                else if (event.type == sf::Event::KeyPressed) {
                    if (event.key.code == sf::Keyboard::Escape)
                        std::exit(0);
                    else m_input_manager.get_pressed(event);
                }
            }
        }
        sf::Time newTime = clock.getElapsedTime();
        sf::Time frameTime = newTime - currTime;
        currTime = newTime;

        accumulator += frameTime;
        EventManager::call_subs();
        while (accumulator >= GameConsts::TIME_PER_FRAME) {
            // bind update to elapsed time
            active_state->update(GameConsts::TIME_PER_FRAME.asSeconds());
            accumulator -= GameConsts::TIME_PER_FRAME;
            time += GameConsts::TIME_PER_FRAME;
        } 
        //window.clear(sf::Color(116, 116, 116));
        window.clear(sf::Color(0, 0, 0));
        active_state->draw(window);
        window.display();
    }
}

InputManager& Game::input_manager() {
    return m_input_manager;
}

SoundManager& Game::sound_manager() {
    return m_sound_manager;
}

MusicManager& Game::music_manager() {
    return m_music_manager;
}

AnimationManager& Game::animation_manager() {
    return m_animation_manager;
}

void Game::swap_states(BaseEvent* other) {
    prev_state = active_state;
    //delete prev_state;
    active_state = new PlayState();
}

void Game::pend_state_swap(State* st) {
    active_state->clear();
    //prev_state = active_state;
    active_state = st;
}

Game::~Game() {
    for(auto& m: m_states)
        delete m.second;
    m_states.clear();
    EventManager::clear();
}