#pragma once
#include <cstdint>
#include <vector>
#include "Geometry.hpp"
#include "Graphics.hpp"

// look into drawing from sfml vertex example
enum ETile: char  { TBrick = 'B', TEmpty = '.', TSteel = 'A', 
    TWater = 'W', TVoid = 'X', TGrass = 'G', TSlide = 'L'};
namespace GameConsts {
    // textures, square
    // gonna invalidate them individually
    const int BrickQty = 4;
    const int BrickRow = 2;
    const int BrickSize = 10;
    const int BlockSize = BrickSize * 2;
    const int BlockQty = 1;
};

// collideble
class Brick {
public:
    Brick(float, float, float, float);
    ~Brick();
    void setVisible();
    bool isVisible();
    bool isEmpty();
    void setEmpty();
    void setInvalid();
    bool getInvalid();
    Vector getPosition();
    void setAnimation(Animation*);
    void animate();
    void fillVertexArray(sf::Vertex*, int, int);
    Animation* animation;
protected:
    bool visible;
    // destructible detection by brick type
    bool empty;
    bool pending_invalidation;
    Rect m_rect;
};

class Tile {
public:
    Tile(float, float, ETile);
    ~Tile();
    bool isDestructible();
    Vector getPosition();
    void update(sf::Vertex*);
protected:
    // somewhere here change texture
    // frame for bricks
    ETile type;
    Rect m_bounding_box;
    std::vector<Brick*> m_bricks;
};