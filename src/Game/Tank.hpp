#pragma once
#include "GameObject.hpp"
#include "Events.hpp"
#include "Geometry.hpp"
#include "Graphics.hpp"

namespace GameConsts {
    const int DefaultHealth = 1;
    const int DefaultPower = 1;
    const int TankSize = 16;
};

class Tank: public CGameObject {
public:
    Tank(const Rect&);
    Tank(const Rect&, Animation*);
    // general function for collision response
    virtual void collision_event(BaseEvent*);
    // create bullet, append to objects
    // different qty for tanks
    virtual void fire();
    virtual void setSpriteTexture(const sf::Texture& tex);
    virtual void draw(sf::RenderWindow&);
    virtual void update();
    virtual ~Tank();

    int health;// hit qty required for destruction. req/ for collis
    int power;//multipliers/nulifier for damage adjustments
    int bullets;//up to TWO bullets at instance
    void handle_movement(BaseEvent*);
    sf::Sprite& getSprite() { return m_sprite; }
protected:
    sf::Sprite m_sprite;
    Rect m_rect;
    Animation* animation;
};

class PTank: public Tank {
public:
    PTank(const Rect&);
    PTank(const Rect&, Animation*);
    virtual ~PTank();
};

class ETank: public Tank {
public:

};