#pragma once
#include "Text.hpp"
#include "GameObject.hpp"
#include "Events.hpp"
#include "ResourceManagers.hpp"
#include "Map.hpp"
#include "Tank.hpp"

namespace GameConsts {
    const int DefaultLives = 12;
};

class State: public CGameObject {
public:
    virtual void draw(sf::RenderWindow&)= 0;
    virtual void update(float)= 0;
    virtual void update() { }
    virtual void clear() { }
    virtual ~State();
};
// show loading...; input; exit;
class GameLoadingState: public State {
public:
    GameLoadingState(float);
    virtual ~GameLoadingState();
    virtual void update(float);
    virtual void draw(sf::RenderWindow&);
protected:
    std::list<Label>::iterator iter;
    Text m_text;
    // append dur, when overfl. carry to nxt
    float duration;
    float total_duration;
    std::list<Label> m_loading_group;
};

// font URL LinuxBiolinux Nimbus Century
class PlayState: public State {
public:
    PlayState();
    virtual ~PlayState();
    virtual void update(float);
    virtual void draw(sf::RenderWindow&);

protected:
    int stage_progress;
    Map m_map;
};

class MenuState: public State {
public:
    MenuState();
    virtual ~MenuState();
    virtual void draw(sf::RenderWindow&);
    virtual void update(float);
    virtual void clear();
    void handleSelectionEvent(BaseEvent*);
protected:
    std::list<Text*>::iterator iter;
    std::list<Text*> menu_group;
};

/*
class PauseState: public State {

};

class StageLoadingState: public State {

};

*/