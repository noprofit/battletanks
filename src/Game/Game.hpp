#pragma once
#include <unordered_map>
#include "Events.hpp"
#include "States.hpp"
#include "Geometry.hpp"
#include "TypeInfo.hpp"
#include "InputManager.hpp"
#include "ResourceManagers.hpp"
// TODO
// - Willfull decision. do NOT use the singleton, just
// forbid the usage of copy/copy-ctr
// - save current state into container
// - std::list<std::pair<*States, Status> > m_states;
// PS mb, mgrs should be a part of States OR they ARE a part
// of game, and it only SHARES them with the States
// (good idea)
namespace GameConsts {
    const int WIDTH = 800;
    const int HEIGHT = 600;
    const int MAX_LEVEL_WIDTH = WIDTH - 200;
    const std::string TITLE = "Battle City";
    // fag
    //const float TIME_PER_FRAME = 1.f / 60.f;
    const sf::Time TIME_PER_FRAME = sf::seconds(1.f / 60.f);
}

class Game {
    Game();
public:
    ~Game();
    Game(const Game&&)= delete; // move removed
    Game& operator=(Game&&)= delete; // mv assignment too;
    Game(const Game&)= delete; // Copy prohibited
    void operator=(const Game&)= delete; // Assignment prohibited

    static Game* instance();
    void run();
    
    void pend_state_swap(State*);
    InputManager& input_manager();
    SoundManager& sound_manager();
    MusicManager& music_manager();
    AnimationManager& animation_manager();
protected:
    void swap_states(BaseEvent*);
    // fore easy search/switch [typeid(State*)]
    std::unordered_map<TypeInfoRef, State*, Hasher, EqualTo> m_states;
    InputManager m_input_manager;
    SoundManager m_sound_manager;
    MusicManager m_music_manager;
    AnimationManager m_animation_manager;
    // actually, can switch state without throwing and event notice
    // the queue goes, state will be handled right before drawing
    // so, can set active state; if typeid is not, switch, delete
    State* active_state;
    State* prev_state;
    Vector m_window_size;
    static Game* _instance;
};