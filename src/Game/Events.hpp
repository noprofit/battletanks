#ifndef EVENTS_H
#define EVENTS_H
// some generalities
/**
    EventManager::subscribe<
    A*, B,
    void (A::* )(Base*)
    >(obj,  &B::func);
*/
// MB i should intruduce type T into base
class BaseEvent {
public:
    virtual ~BaseEvent() { }
};
// lets say templated, so we wont have to introduced
// any custom type
template<class T>
class KeyPressedEvent: public BaseEvent {
public:
    KeyPressedEvent(T t): button(t) { }
    virtual ~KeyPressedEvent() { }
    // const referencing get
    T& getButton() { return button; }
private: // immutable after creation
    T button;
};


class GameStartEvent: public BaseEvent {
public:
    virtual ~GameStartEvent() { }

};

class AppCloseEvent: public BaseEvent {

};

class CollisionEvent: public BaseEvent {

};

class BulletFiredEvent: public BaseEvent {

};

class BulletHitEvent: public BaseEvent {

};
#endif