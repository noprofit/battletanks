#include "Tank.hpp"
#include "Game.hpp"
#include "EventManager.hpp"

Tank::~Tank() { }

Tank::Tank(const Rect& rect)
 : m_rect(rect), m_sprite(),
 health(GameConsts::DefaultHealth),
 power(GameConsts::DefaultPower), bullets(0) { 
    m_sprite.setPosition(m_rect.pos);
    EventManager::subscribe<
    Tank*, KeyPressedEvent<Button>,
    void (Tank::* )(BaseEvent*)
    >(this, &Tank::handle_movement);
}

Tank::Tank(const Rect& rect, Animation* anim)
 : m_rect(rect), animation(anim), m_sprite(),
 health(GameConsts::DefaultHealth),
 power(GameConsts::DefaultPower), bullets(0) { 
    m_sprite.setPosition(m_rect.pos);
    m_sprite.setTextureRect(animation->current()->rect);
}

void Tank::setSpriteTexture(const sf::Texture& tex) { m_sprite.setTexture(tex); }

void Tank::draw(sf::RenderWindow& wnd) {
    wnd.draw(m_sprite);
}

void Tank::update() {
    m_sprite.setPosition(m_rect.pos);
    m_sprite.setTextureRect(m_rect);
}

void Tank::fire() { }
void Tank::collision_event(BaseEvent* ev) { }
void Tank::handle_movement(BaseEvent* ev) {
/*
So, have to receive event w/ movement here
Make an approximation.
Try collision call
Revert if ated shit
THAN, when A-><-B next obj tries to move to the same
cell as previous one, he will be late for it, but
the first one had succeeded.

*/
}

PTank::PTank(const Rect& rect, Animation* anim)
 : Tank(rect, anim) { }
 
PTank::PTank(const Rect& rect): Tank(rect) { }

PTank::~PTank() { }