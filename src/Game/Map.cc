#include "Map.hpp"
#include "Game.hpp"
#include <string>
#include <fstream>
#include <stdexcept>
#include <iterator>
#include <cassert>
#include <cstdio>
// initializes tiles
// load texture
// loads level
Map::Map(int _level): width(0), height(0), m_vertices(), level(_level),
 m_tiles(), m_texture(Game::instance()->animation_manager().getTexture()) {
    std::string path = std::string("../res/Maps/") + std::to_string(level) + ".lvl";
    std::ifstream file(path, std::ios::binary);
    // map size is fluid, to level data. capped at gameconstants data
    if (!file.is_open())
        throw std::runtime_error((std::string("Error, file failed to open: ") + path).c_str());

    std::string level_data;
    std::getline(file, level_data);
    width = level_data.size();
    height = std::count(std::istreambuf_iterator<char>(file),
        std::istreambuf_iterator<char>(), '\n');
    m_tiles.reserve(width * height);
    // resize vertices
    m_vertices.setPrimitiveType(sf::Triangles);
    // 6 vertices, 4 quds within SINGLE tile
    m_vertices.resize(width * height * 6 * 4);
    // for width/height temporaries
    file.close();
}

void Map::init() {
    std::string path = std::string("../res/Maps/") + std::to_string(level) + ".lvl";
    std::ifstream file(path, std::ios::binary);
    // map size is fluid, to level data. capped at gameconstants data
    
    if (!file.is_open())
        throw std::runtime_error((std::string("Error, file failed to open: ") + path).c_str());

    int i = 0, j = 0;
    // level_data initally set
    std::string level_data;
    std::getline(file, level_data);
    while (std::getline(file, level_data)) {
        for(auto& c: level_data)
            if (c != '\n' && c != ' ' && j != 26)
            // each character's single tile's data;
                m_tiles.emplace_back(
                    i++ * GameConsts::BlockSize,
                    j * GameConsts::BlockSize,
                    static_cast<ETile>(c)
                );
        j++; i = 1;
    }
    
    file.close();
}

Map::~Map() { }

void Map::draw(sf::RenderWindow& wnd) {
    int i = 0;
    for(auto& t: m_tiles) {
        t.update(&m_vertices[i]);
        i += 4 * 6;
    }
    wnd.draw(m_vertices, &m_texture);
}

Tile& getAt(Vector vect) {

}

void Map::loadLevel(int level) {

}

void Map::clear() {
    m_tiles.clear();
}