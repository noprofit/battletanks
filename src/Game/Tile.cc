#include "Tile.hpp"
#include "Game.hpp"
#include <cstdio>
#include <cassert>
#include <type_traits>
#include <iterator>

Tile::Tile(float x, float y, ETile _type): m_bounding_box(Rect(
 x, y,
 static_cast<float>(GameConsts::BrickQty * GameConsts::BrickSize),
 static_cast<float>(GameConsts::BrickQty * GameConsts::BrickSize)
 )),type(_type), m_bricks()  {

    m_bricks.reserve(GameConsts::BrickQty);
    for(auto j = 0; j < GameConsts::BrickQty / GameConsts::BrickRow; j++)
        for(auto i = 0; i < GameConsts::BrickQty / GameConsts::BrickRow; i++) {
            auto temp = 
                new Brick(
                    x + GameConsts::BrickSize * i,
                    y + GameConsts::BrickSize * j,
                    static_cast<float>(GameConsts::BrickSize), 
                    static_cast<float>(GameConsts::BrickSize)
                );
                    std::string idx;
                    if(_type == TBrick) idx = "brick_block";
                    else if (_type == TEmpty || _type == TVoid) idx = "empty_block";
                    else idx = "steel_block";
            temp->setAnimation(&(Game::instance()->animation_manager()[
                idx
                ])
            );
            m_bricks.push_back(temp);
        }

}

void Tile::update(sf::Vertex* quad) {
    int  i, j;
    i = j = 0;
    for(auto& b: m_bricks) {      
        b->fillVertexArray(quad, i, j);
        quad = quad + 6;
        if (++i == 2) {
            if (++j == 2) j = 0;
            i = 0;
        }
    }
}

Tile::~Tile() {
    for(auto& b: m_bricks)
        delete b;
    m_bricks.clear();
}

bool Tile::isDestructible() {
    if (type == TBrick)
        return true;
    return false; 
}

Vector Tile::getPosition() { return m_bounding_box.pos; }

void Brick::fillVertexArray(sf::Vertex* quad, int i, int j) {
    sf::FloatRect t_rect = animation->current()->rect;
    t_rect.width = t_rect.width / 2;
    t_rect.height = t_rect.height / 2;
    t_rect.left = t_rect.left + i * t_rect.width;
    t_rect.top = t_rect.top + j * t_rect.height;
    quad[0].position = m_rect.pos;
    quad[1].position = sf::Vector2f(m_rect.pos.x + m_rect.width, m_rect.pos.y);
    quad[2].position = sf::Vector2f(m_rect.pos.x, m_rect.pos.y + m_rect.height);
    quad[3].position = sf::Vector2f(m_rect.pos.x, m_rect.pos.y + m_rect.height);
    quad[4].position = sf::Vector2f(m_rect.pos.x + m_rect.width, m_rect.pos.y);
    quad[5].position = sf::Vector2f(m_rect.pos.x + m_rect.width, m_rect.pos.y + m_rect.height);
    // texture
    quad[0].texCoords = sf::Vector2f(t_rect.left, t_rect.top);
    quad[1].texCoords = sf::Vector2f(t_rect.left + t_rect.width, t_rect.top);
    quad[2].texCoords = sf::Vector2f(t_rect.left, t_rect.top + t_rect.height);
    quad[3].texCoords = sf::Vector2f(t_rect.left, t_rect.top + t_rect.height);
    quad[4].texCoords = sf::Vector2f(t_rect.left + t_rect.width, t_rect.top);
    quad[5].texCoords = sf::Vector2f(
        t_rect.left + t_rect.width, 
        t_rect.top + t_rect.height
    );
}

void Brick::setAnimation(Animation* anim) {
    animation = anim;
}

Brick::Brick(float x, float y, float w, float h): m_rect(x, y, w, h), visible(true),
 animation(nullptr), empty(false) { }

Vector Brick::getPosition() { return m_rect.pos; }

// made empty by collision trigger
// or response
void Brick::setVisible() { visible = true; }
bool Brick::isVisible() { return visible; }


Brick::~Brick() { }
bool Brick::isEmpty() { return empty; }
void Brick::setEmpty() { empty = true; }
void Brick::setInvalid() { pending_invalidation = true; }
bool Brick::getInvalid() { return pending_invalidation; }