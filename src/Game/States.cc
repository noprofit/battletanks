#include "States.hpp"
#include "Text.hpp"
#include "Game.hpp"
#include "Geometry.hpp"
#include "EventManager.hpp"
#include <cassert>
#include <cstdio>
#include <iterator>

GameLoadingState::GameLoadingState(float d): m_loading_group(),
 total_duration(d), duration(0.f), m_text() {
    m_loading_group.push_back({"Loading.", 1.f});
    m_loading_group.push_back({"Loading..", 1.f});
    m_loading_group.push_back({"Loading...", 1.f});
    iter = m_loading_group.begin();
    m_text.setText(iter->data);
    m_text.setPosition(Vector(
        GameConsts::WIDTH / 2 - (m_text.text().getGlobalBounds().width / 2),
        GameConsts::HEIGHT / 2 - (m_text.text().getGlobalBounds().height / 2)
    ));
    duration = iter->duration;
}

GameLoadingState::~GameLoadingState() {
    m_loading_group.clear();
}

void GameLoadingState::update(float dt) {
    total_duration -= dt;
    if (total_duration <= 0.f) {
        Game::instance()->pend_state_swap(new MenuState());
        return;
    }
    if ((duration -= dt) <= 0.f) {
        if (++iter == m_loading_group.end())
            iter = m_loading_group.begin();
        m_text.setText(iter->data);
        duration += iter->duration;
    }        
}

void GameLoadingState::draw(sf::RenderWindow& wnd) {
    m_text.draw(wnd);
    CGameObject::draw(wnd);
}

MenuState::MenuState(): menu_group() {
    float offset = 0.f;
    for (auto& m: {"One Player", "Two Players", "Constructor"}) {
        Text* t = new Text();
        t->setText(m);
        t->setFont(std::string("LinBiolinum_RI"));
        t->setPosition(Vector(
            GameConsts::WIDTH / 2 - t->text().getGlobalBounds().width / 2,
            GameConsts::HEIGHT / 2.3f - t->text().getCharacterSize() + offset
        ));
        offset += t->text().getCharacterSize();
        menu_group.push_back(t);
    }
    iter = menu_group.begin();
    (*iter)->setColor(sf::Color::Green);
    EventManager::subscribe<
        MenuState*, KeyPressedEvent<Button>,
        void (MenuState::* )(BaseEvent*)
        >(this, &MenuState::handleSelectionEvent);
}

MenuState::~MenuState() {
    for (auto& t: menu_group)
        delete t;
    menu_group.clear();
}

void MenuState::draw(sf::RenderWindow& wnd) { 
    for (auto& m: menu_group)
        m->draw(wnd);
}

void MenuState::update(float dt) {

}

void MenuState::clear() {
    EventManager::unsubscribe<MenuState, KeyPressedEvent<Button> >();
}

void MenuState::handleSelectionEvent(BaseEvent* keyPressedEvent) {
    KeyPressedEvent<Button>* ev = dynamic_cast<KeyPressedEvent<Button>* >(keyPressedEvent);
    (*iter)->setColor(sf::Color::White);
    (*iter)->setStyle(sf::Text::Bold);
    if (ev->getButton().action() == "V+")
        if (iter == menu_group.begin())
            iter = std::prev(menu_group.end());
        else --iter;
    else if (ev->getButton().action() == "V-")
        if (std::next(iter) == menu_group.end())
            iter = menu_group.begin();
        else ++iter;
    (*iter)->setColor(sf::Color::Green);
    if (ev->getButton().action() == "Fire" || ev->getButton().action() == "Enter") {
        (*iter)->setStyle(sf::Text::Italic);
        (*iter)->setColor(sf::Color::Yellow);
        //EventManager::fire(new GameStartEvent());
        Game::instance()->pend_state_swap(new PlayState());
    }
}

// init map
// init animation
PlayState::PlayState(): 
 stage_progress(1), m_map(stage_progress) {
    auto& m_animation_manager = Game::instance()->animation_manager();
    m_animation_manager.addAnimation("empty_block");
    m_animation_manager["empty_block"].addFrame(16, 272, 16, 16, 0.f);
    m_animation_manager.addAnimation("brick_block");
    m_animation_manager["brick_block"].addFrame(0, 256, 16, 16, 0.f);
    m_animation_manager.addAnimation("steel_block");
    m_animation_manager["steel_block"].addFrame(0, 272, 16, 16,  0.f);
    m_map.init();
    m_animation_manager.addAnimation("player_tank");
    m_animation_manager["player_tank"].addFrame(3, 5, 26, 26, 0.f);
    auto tank = new PTank(
        {static_cast<float>(GameConsts::BlockSize * 11),
        static_cast<float>(GameConsts::BlockSize * 25),
        static_cast<float>(GameConsts::TankSize),
        static_cast<float>(GameConsts::TankSize)},
        &m_animation_manager["player_tank"]
    );
    tank->setSpriteTexture(m_animation_manager.getTexture());
    appendObject(tank);
    /*
    Two ways i see this to go down. 
    1. is to "Nest" objects. And run/intercept things by hierarchically
    calling update/draw to sublings.
    2. Is to DEttach object fully. Just preserv it inside container.
    And run things based on event/subs line.
    3. And third one. I think good idea could be to mix/match things.
    The logic regarding state SHOULD be inside this state. Object management
    can be handled by the objects themselves (this is where event thing comes in).
    And yes, use hierarchicall update nesting from state.*/
}

PlayState::~PlayState() { 

}

void PlayState::update(float dt) {
    //for(auto& o: m_objects)
       // o->update();
}

void PlayState::draw(sf::RenderWindow& wnd) {
    // use game loading state to show Level 1 label
    // instead of playstate.
    CGameObject::draw(wnd);
    m_map.draw(wnd);
    //tank->draw(wnd);*/
}

State::~State() { }