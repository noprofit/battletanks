#pragma once
#include <SFML/Graphics.hpp>
#include <list>
#include <cstdio>
#include <cassert>
#include "Geometry.hpp"

// defines base for composit
class CGameObject;
typedef std::list<CGameObject*>::iterator CGameIter;
typedef std::list<CGameObject*>::const_iterator CGameIterConst;

class CGameObject {
public:
    CGameObject() { }
    CGameObject(const CGameObject& other): 
        m_objects(other.m_objects) { }
    virtual ~CGameObject() { }

    virtual void update()= 0;

    virtual void draw(sf::RenderWindow& wnd) {
        for(auto& _obj: m_objects)
            _obj->draw(wnd);
    }

    CGameIter begin() { return m_objects.begin(); }
    CGameIter end() { return m_objects.end(); }
    CGameIterConst cbegin() { return m_objects.cbegin(); }
    CGameIterConst cend() { return m_objects.cend(); }
    void appendObject(CGameObject* obj) {
        m_objects.push_back(obj);
        obj->setParent(this);
    }

    void setParent(CGameObject* obj) {
        this->m_parent = obj;
    }

    CGameObject* getParent() {
        return m_parent;
    }
    // template overloads to return EXACT pointer type objects
    // or i can expermiment with polymorphicality
    template<typename T>
    T* findByType() {
        for(auto& o: m_objects)
            if(typeid(T).hash_code() == typeid(*o).hash_code())
                return dynamic_cast<T*>(o);
        return nullptr;
    }
protected:
    std::list<CGameObject*> m_objects;
    CGameObject* m_parent;
};