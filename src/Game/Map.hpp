#pragma once
#include "Tile.hpp"
// can omit D. since there is no real abstraction
// over level, besides raw!temporary! data
// also map, is rather part of Game than Engine
class Map {
public:
    // hardcoded tile resource, std::string
    Map(int level);
    void init();
    ~Map();
    // calls function to EXTRACT verticies from brick
    void draw(sf::RenderWindow&);
    Tile& getAt(Vector);
    // if keeping map in memory permamently
    void loadLevel(int);
protected:
    std::vector<Tile> m_tiles;
    sf::VertexArray m_vertices;
    const sf::Texture& m_texture;
    // if map will be persistent, clear current
    // cell data
    void clear();
    int width;
    int height;
    int level;
};