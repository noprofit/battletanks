#include <iostream>
#include "Game.hpp"

int main() {
    Game* game = Game::instance();
    game->run();

    return 0;
}