#include "ResourceManagers.hpp"
#include <cassert>
#include <stdexcept>
#include <iostream>

SoundManager::SoundManager(): m_sound(), 
    m_sound_buffer() { }

SoundManager::~SoundManager() { 
   m_sound_buffer.clear();
}

void SoundManager::loadFromFile(const std::string& name, const std::string& path) {
    assert(("Error while loading sound, already exists", m_sound_buffer.find(name) == m_sound_buffer.end()));
    m_sound_buffer[name] = sf::SoundBuffer();
    if (!m_sound_buffer[name].loadFromFile(path))
        throw std::runtime_error(("Error while loading file: " + path).c_str());
}

void SoundManager::play(const std::string&) { }

MusicManager::MusicManager(): m_music() { }

MusicManager::~MusicManager() { };

void MusicManager::loadFromFile(const std::string& name, const std::string& path) {
    if (!m_music.openFromFile(path))
        throw std::runtime_error(("Error while loading music from file: " + path).c_str());
}

void MusicManager::play_queue() { }
void MusicManager::play(const std::string&) { }
void MusicManager::play_loop(const std::string&) { }

AnimationManager::AnimationManager()
 : m_textures(), m_animations() { }

AnimationManager::~AnimationManager() { }

void AnimationManager::loadTextureFromFile(std::string path, const std::string& name) {
    if (!m_textures[name].loadFromFile(path))
        throw std::runtime_error(("Failed loading texture from file: " + path).c_str());
}

const sf::Texture& AnimationManager::getTexture(const std::string& name) {
    if (m_textures.find(name) == m_textures.end())
        throw std::runtime_error(("Texture: " + name + " not found.").c_str());
    return m_textures[name]; 
}

void AnimationManager::addAnimation(const std::string& name) {
    assert(m_animations.find(name) == m_animations.end());//exists
    m_animations[name] = Animation();
}

void AnimationManager::addAnimation(const char* name) {
    addAnimation(std::string(name));
}

Animation& AnimationManager::operator[](const std::string& idx) {
    assert(m_animations.find(idx) != m_animations.end());
    return m_animations[idx];
}
Animation& AnimationManager::operator[](const char* idx) {
    return m_animations[std::string(idx)];
}