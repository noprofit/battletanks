#pragma once
#include "Events.hpp"
class Delegate {
    struct BaseHolder {
        virtual ~BaseHolder() { }
        virtual void execute(BaseEvent*)= 0;
        virtual void Release()= 0;
    };

    template<typename Object, typename Callable>
    struct Holder: BaseHolder { //<Object, void (Object::*)(Event)>
        //typedef void (Object::*m_p)(Event);
        Holder(Object obj, Callable call):
            object(obj), callable(call) { }
        // probably, destructor must be method-wrapped
        virtual ~Holder() { };
        void execute(BaseEvent* ev) { (object->*callable)(ev); }
        // remove,clean
        void Release() { }
        private:
            Object object;
            Callable callable;
    };
    BaseHolder* holder;
public:
    void execute(BaseEvent* ev) { holder->execute(ev); }
    Delegate(): holder(nullptr) { }
    ~Delegate() { delete holder; }
    template<typename Object, typename Callable>
    Delegate(Object obj, Callable call):
        holder(new Holder<Object, Callable>(obj, call)) { }
};