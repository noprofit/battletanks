#pragma once
#include <map>
#include <string>
#include "Graphics.hpp"
#include <SFML/Audio.hpp>

/**
 * Good idea is to separate ENGINE logic from GAME logic
 **/
class SoundManager {
public:
    SoundManager();//loads audio into map
    ~SoundManager();
    void loadFromFile(const std::string&, const std::string&);
    void play(const std::string&);
    void getState(const std::string&);
    void changeState(const std::string&);
private:
    sf::Sound m_sound;
    std::map<std::string, sf::SoundBuffer> m_sound_buffer;
};

class MusicManager {
public:
    MusicManager();//loads audio into map
    ~MusicManager();
    void loadFromFile(const std::string&, const std::string&);
    void play_queue();
    void play(const std::string&);
    void play_loop(const std::string&);//w/ default
    void getState(const std::string&);
    void changeState(const std::string&);
private:
    sf::Music m_music;
};

// lets have predefined TRANSFORMED animation pull aka h_y_x - heavy-yellow-x
class AnimationManager {
public:
    AnimationManager();
    ~AnimationManager();
    void loadTextureFromFile(std::string, const std::string& name = "default");
    void addAnimation(const std::string&);
    void addAnimation(const char*);
    Animation& operator[](const std::string&);
    Animation& operator[](const char*);
    const sf::Texture& getTexture(const std::string& name = "default");
private:
    std::map<std::string, Animation> m_animations;
    std::map<std::string, sf::Texture> m_textures;
};