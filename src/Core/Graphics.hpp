#pragma once
#include "GameObject.hpp"
#include "Geometry.hpp"
// single rectangle w/ duration and texture pointer
// sf::Sprite + Rectangle
struct Frame {
    Frame(float, float, float, float, float);
    Frame(const Frame&);
    float duration;
    Rect rect;
};
// animation is a set of frames
// and backwards iteration(list): Animation tank_move_up/tank_move_down
//  + rotation(tank Direction)
typedef std::list<Frame>::iterator FrameIter;
typedef std::list<Frame>::const_iterator FrameIterConst;

class Animation {
public:
    Animation();
    Animation(const Animation& other);
    ~Animation();

    void rotateUp();
    void rotateDown();
    void rotateLeft();
    void rotateRight();
    float getAngle();
    void adv_forward();
    void adv_backward();
    Animation clone();
    void addFrame(float, float, float, float, const float&);
    void next();
    void prev();
    void reset();
    FrameIter current();
    FrameIter begin();
    FrameIter end();
    FrameIterConst cbegin();
    FrameIterConst cend();
protected:
    float angle;
    float curr_duration;
    FrameIter _current;
    std::list<Frame> m_frames;
};