#include "InputManager.hpp"
#include "EventManager.hpp"
#include <type_traits>
#include <iostream>

Button::Button() { }

Button::Button(std::string name, std::string action,
    sf::Keyboard::Key key): pressed(false),
    _n(name), _a(action), _k(key) { }

Button::Button(const Button& other): pressed(other.pressed),
    _n(other._n), _a(other._a), _k(other._k) { }

Button::Button(const Button&& other): pressed(other.pressed),
    _n(other._n), _a(other._a), _k(other._k) { }

Button& Button::operator=(const Button& other) {
    _n = other._n;
    _a = other._a;
    _k = other._k;
    return *this;
}

sf::Keyboard::Key  Button::key() { return _k; }
std::string Button::name() { return _n; }
std::string Button::action() { return _a; }

InputManager::InputManager() {


}

void InputManager::register_input(std::string name, std::string action, 
    sf::Keyboard::Key key) {
    auto it = m_control_keys.find(name);
    if (it == end())
        m_control_keys[name] = Button(name, action, key);
}

void InputManager::get_pressed(sf::Event& ev) {
    for(auto& it: *this)
        if (ev.key.code == it.second.key())
            EventManager::fire(new KeyPressedEvent<Button>(it.second));
}

InputManagerIter InputManager::begin() { return m_control_keys.begin(); }
InputManagerIter InputManager::end() { return m_control_keys.end(); }
InputManagerConstIter InputManager::cbegin() { return m_control_keys.cbegin(); }
InputManagerConstIter InputManager::cend() { return m_control_keys.cend(); }