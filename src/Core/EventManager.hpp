#pragma once
#include <utility>
#include <functional>
#include <cassert>
#include <iostream>
#include <algorithm>
#include <unordered_map>
#include "TypeInfo.hpp"
#include "Delegate.hpp"
#include "GameObject.hpp"

class BaseEvent;
typedef std::unordered_map<TypeInfoRef, Delegate*, Hasher, EqualTo> subscriber;
typedef std::pair<TypeInfoRef, Delegate*> sub_pair;

class EventManager {
public:
    static void clear();
    // create new notifyable
    static void fire(BaseEvent* event) {
        m_events.push_back(event);
    }
    // invalidate m_events after a corresponding Event been handled
    // (all subscribers notified, called)
    static void call_subs();
    /**
     * Event<int>* event = new Event<int>(data);
     * EventManager::subscribe<A,B,void (A::* )(B)>(a, &A::run);
     * */
    template<typename Object, typename Event, typename Handler>
    static void subscribe(Object object, Handler callable) {
        auto it = m_subscribers.find(typeid(Event));
        if (it != m_subscribers.end())
            if (it->second.find(typeid(*object)) != it->second.end())
                throw std::runtime_error("Already subscribed.");
        
        m_subscribers[typeid(Event)].insert(
            sub_pair(typeid(*object), new Delegate(object, callable))
            );
    }

    template<typename Object, typename Event>
    static void unsubscribe() {
        auto it_fst = m_subscribers.find(typeid(Event));

        if (it_fst != m_subscribers.end()) {
            auto it = it_fst->second.find(typeid(Object));
            if (it != m_subscribers.begin()->second.end()) {
                delete it->second;
                it->second = nullptr;
                it_fst->second.erase(typeid(Object));
            }

            m_subscribers.erase(typeid(Event));
            return;
        }
        throw std::runtime_error("Nothing to unsubscribe from.");
    }
private:
    static std::list<BaseEvent*> m_events;
    static std::unordered_map<TypeInfoRef, subscriber,  Hasher, EqualTo> m_subscribers;
};