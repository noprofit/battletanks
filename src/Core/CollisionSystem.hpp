#pragma once
class CollisionSystem {
public:
    void Register(Collidable*);
    // void register(CGameObject* o) => o->getPosition
    void check_all(); // since obj are pointers, the
    // position will be checked, and when invalidated
    // objects will be destroyed
    void unregister(BaseEvent*);//typeid
protected:
    std::vector<CGameObject*> objects;
    std::vector<std::pair<typeid, CGameObject*> >
    std::unordered_map<typeid, CGameObject*>
};