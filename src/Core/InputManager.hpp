#pragma once
#include <SFML/Graphics.hpp>
#include <string>
#include <map>
#include <list>

struct Button {
    Button();
    Button(std::string name, std::string action, sf::Keyboard::Key);
    Button(const Button&);
    Button(const Button&&);
    Button& operator=(const Button& other);
    
    std::string name();
    std::string action();
    sf::Keyboard::Key key();
    bool pressed;
private:
    std::string _n;
    std::string _a;
    sf::Keyboard::Key _k;
};

typedef std::map<std::string, Button >::iterator InputManagerIter;
typedef std::map<std::string, Button >::const_iterator InputManagerConstIter;

class InputManager {
public:
    InputManager(); // keyboard defs, action, control
    void register_input(std::string, std::string, sf::Keyboard::Key);
    void get_pressed(sf::Event&);
    InputManagerIter begin();
    InputManagerIter end();
    InputManagerConstIter cbegin();
    InputManagerConstIter cend();
private:
    // map pressed/released to the specific key
    // m_control_keys
    std::map<std::string, Button> m_control_keys;
};