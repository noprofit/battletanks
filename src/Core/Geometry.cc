#include "Geometry.hpp"

Vector::Vector(): x(), y() { }

Vector::Vector(float _x, float _y): x(_x), y(_y) { }

Vector::Vector(int _x, int _y): x(static_cast<float>(_x)), y(static_cast<float>(_y)) { }

Vector::Vector(const Vector& other) {
    x = other.x;
    y = other.y;
}

Vector::Vector(const Vector&& other) {
    x = other.x;
    y = other.y;
}

Vector& Vector::operator=(const Vector& other) {
    x = other.x;
    y = other.y;
    return *this;
}

Vector& Vector::operator=(const Vector&& other) {
    x = other.x;
    y = other.y;
    return *this;
}

float Vector::length() { return std::sqrt(x*x + y*y); }

void Vector::normalize() { }

bool Vector::normalized() { }

bool Vector::normalTo(const Vector& other) {
    return (0.f == (other.x * x + other.y + y));
}

Vector Vector::normal() {
    return Vector(y * -1.f, x);
}

Vector::operator sf::Vector2i() const {
    return sf::Vector2i(
        static_cast<int>(x),
        static_cast<int>(y)
    );
}

Vector::operator sf::Vector2f() const {
    return sf::Vector2f(x, y);
}

Rect::Rect(): width(), height() { }

Rect::Rect(int x, int y, int w, int h): width(w), height(h),
 pos(x, y) { }

Rect::Rect(float x, float y, float w, float h): width(w), height(h),
 pos(x, y) { }

Rect::Rect(Vector a, Vector b): width(a.length()), 
 height(b.length()) { }

Rect::Rect(Vector a, Vector b, Vector _pos): width(a.length()), 
 height(b.length()), pos(_pos) { }
// mostly, get positional vector as SFML entity
Rect::operator sf::IntRect() const {
    return sf::IntRect(
        static_cast<int>(pos.x),
        static_cast<int>(pos.y),
        static_cast<int>(width),
        static_cast<int>(height)
    );
}
Rect::operator sf::FloatRect() const {
    return sf::FloatRect(pos.x, pos.y, width, height);
}