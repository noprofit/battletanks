#include "Text.hpp"

Label::Label(std::string _d, float dur)
 : data(_d), duration(dur) { }

Text::Text(): m_text(), data("Hello World") {
    auto base_path = "../res/Fonts/";
    auto ext = ".otf";
    for (auto& f: { "LinBiolinum_K", "LinBiolinum_R",
        "LinBiolinum_RB", "LinBiolinum_RI", "NotoGothic"}) {
        sf::Font* font = new sf::Font();
        if (font->loadFromFile(std::string(base_path) + f + ext))
            fallback_fonts[std::string(f)] = font;
        else throw std::runtime_error("Error loading font");
    }
    font = fallback_fonts["LinBiolinum_K"];
    m_text.setPosition(0.f, 0.f);
    m_text.setFont(*font);
    m_text.setCharacterSize(32);
    m_text.setStyle(sf::Text::Bold);
    m_text.setFillColor(sf::Color::White);
    m_text.setString(data);
}

Text::Text(const Text& other): font(other.font),
 fallback_fonts(other.fallback_fonts),
 m_text(other.m_text), data(other.data) {

}

bool Text::loadFontFromFile(const std::string& name, const std::string& path) { 
    sf::Font* t_font = new sf::Font();
    if (t_font->loadFromFile(path))
        if (fallback_fonts.find(name) == fallback_fonts.end())
            fallback_fonts[name] = font;
        else throw std::runtime_error("Font already exist");
    else throw std::runtime_error("Error loading font");
}

void Text::setFont(const std::string& name) {
    if (fallback_fonts.find(name) != fallback_fonts.end())
        font = fallback_fonts[name];
    // mb, instead set to fallback font of choise etc
    else throw std::runtime_error("Font not found");
    m_text.setFont(*font);
}

const sf::Font* Text::getCurrentFont() { return m_text.getFont(); }

const sf::Font* Text::findFont(const std::string& name) {
    auto it = fallback_fonts.find(name);
    if (it != fallback_fonts.end())
        return it->second;
    return nullptr;
}

void Text::setPosition(Vector vec) {
    m_text.setPosition(vec.x, vec.y);
}

sf::Vector2f Text::getPosition() {
    return m_text.getPosition();
}

void Text::setText(const char* text) {
    setText(std::string(text));
}

void Text::setText(const std::string& text) {
    m_text.setString(text);
}

std::string Text::getText() {
    return m_text.getString();
}

void Text::draw(sf::RenderWindow& wnd) {
    wnd.draw(m_text);
}

void Text::setColor(sf::Color cl) {
    m_text.setColor(cl);
}

void Text::setStyle(sf::Text::Style style) {
    m_text.setStyle(style);
}

Text::~Text() {
    for(auto& f: fallback_fonts)
        delete f.second;
    fallback_fonts.clear();
}