#pragma once
#include <cmath>
#include <SFML/Graphics.hpp>

class Vector {
public:
    float x;
    float y;
    Vector();
    Vector(int x, int y);
    Vector(float x, float y);
    Vector(const Vector&);
    Vector(const Vector&&);
    Vector& operator=(const Vector&);
    Vector& operator=(const Vector&&);
    // unit
    void normalize();
    bool normalized();
    bool normalTo(const Vector&);
    Vector normal();
    float length();
    operator sf::Vector2f() const;
    operator sf::Vector2i() const;
};

// rectangle has position Vector,
class Rect {
public:
    Rect();
    Rect(int x, int y, int w, int h);
    Rect(float x, float y, float w, float h);
    Rect(Vector width, Vector height);
    Rect(Vector width, Vector height, Vector position);
    //Rect(const Rect&);
    //Rect(const Rect&&);
    //Rect& operator(const Rect&);
    //Rect& operator(const Rect&&);

    float width;
    float height;
    Vector pos;
    operator sf::FloatRect() const;
    operator sf::IntRect() const;
};