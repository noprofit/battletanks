#pragma once
#include <SFML/Graphics.hpp>
#include <map>
#include <list>
#include <string>
#include "Geometry.hpp"

struct Label {
    Label(std::string, float);
    const std::string data;
    const float duration;
};

class Text {
public:
    Text();
    Text(const Text&);
    ~Text();

    bool loadFontFromFile(const std::string&, const std::string&);
    void setFont(const std::string&);
    const sf::Font* getCurrentFont();
    const sf::Font* findFont(const std::string&);
    void setPosition(Vector);
    void setColor(sf::Color);
    void setStyle(sf::Text::Style);
    sf::Vector2f getPosition();
    void setText(const std::string&);
    void setText(const char*);
    std::string getText();
    void draw(sf::RenderWindow&);
    const sf::Text text() { return m_text; }
private:
    std::string data;
    sf::Text m_text;
    sf::Font* font;
    std::map<const std::string, sf::Font*> fallback_fonts;
};