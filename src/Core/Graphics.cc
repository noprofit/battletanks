#include "Graphics.hpp"
#include <iterator>

Frame::Frame(float top, float left, float width, float height, float _duration)
 :duration(_duration),
 rect(top, left, width, height) { }

Frame::Frame(const Frame& other) :duration(other.duration), 
    rect(other.rect) { }

Animation::Animation(): m_frames(), _current(m_frames.end()) { }

Animation::Animation(const Animation& other)
 : m_frames(other.m_frames) { }

FrameIter Animation::current() { return _current; }
FrameIter Animation::begin() { return m_frames.begin(); }
FrameIter Animation::end() { return m_frames.end(); }
FrameIterConst Animation::cbegin() { return m_frames.cbegin(); }
FrameIterConst Animation::cend() { return m_frames.cend(); }
void Animation::rotateUp() { }
void Animation::rotateDown() { }
void Animation::rotateLeft() { }
void Animation::rotateRight() { }
float Animation::getAngle() { }
void Animation::adv_forward() { }
void Animation::adv_backward() { }
Animation Animation::clone() { }
void Animation::addFrame(float x, float y, float w, float h, const float& duration) {
    m_frames.emplace_back(x, y, w, h, duration);
    _current = std::prev(m_frames.end());
}
void Animation::next() { if (std::next(current()) != end()) _current = std::next(_current); }
void Animation::prev() { if (current() != begin()) _current = std::prev(_current); }
void Animation::reset() { _current = m_frames.begin(); }
Animation::~Animation() { }