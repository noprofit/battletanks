#include "EventManager.hpp"
#include <stdexcept>
#include <iostream>

std::list<BaseEvent*> EventManager::m_events = {};
std::unordered_map<TypeInfoRef, subscriber, Hasher, EqualTo> EventManager::m_subscribers = {};
void EventManager::call_subs() {
    // have to invalidate m_events[] after call been made
    // for now REGARDLESS of success/failur in the action
    auto end = m_subscribers.end();
    for(auto& ev: m_events)
        if (m_subscribers.find(typeid(*ev)) != end)
            for(auto& sub: m_subscribers[typeid(*ev)]) {
                auto sz = m_subscribers[typeid(*ev)].size();
                if (sz--) sub.second->execute(ev);
            }
    
    m_events.clear();
}

void EventManager::clear() {
    for(auto& e: m_events)
        delete e;
    m_events.clear();
    for(auto& s: m_subscribers) {
        for(auto& l: s.second)
            delete l.second;
        s.second.clear();
    }
    m_subscribers.clear();
}
