#define BOOST_TEST_MODULE utility_test
#include "EventManager.hpp"
#include <boost/test/included/unit_test.hpp>
#include <type_traits>
#include <functional>
#include <cassert>
#include <list>
BOOST_AUTO_TEST_CASE(first_test) {
    std::cout << "\033[32m*** first_test \033[0m\n";
    /*BOOST_CHECK(Property<int>());
    BOOST_CHECK(Property<char*>());
    BOOST_CHECK(Property<int>(7));
    BOOST_CHECK(Property<const char*>("allahu akbar"));
    BOOST_CHECK(Property<int>(Property<int>(23)) == Property<int>(Property<int>(23)));*/
}
class A {
public:
    virtual ~A() { }
};
class B : public A {
public:
    B():i(8) { std::printf("before: %d\n", i); };
    virtual ~B() { }
    void runnable(BaseEvent* p) {
        assert((typeid(*p).hash_code() == typeid(KeyPressedEvent<int>).hash_code(), "KeyPr type missmatched."));
        std::printf("im runnable from B\n");
        i = 10;
        //std::printf("after %d", i);
    }
    int i;
};
class C {
public:
    C():i(8) { std::printf("before: %d\n", i); };
    virtual ~C() {}
    void executable(BaseEvent* p) {
assert((typeid(*p).hash_code() == typeid(CollisionEvent).hash_code(), "KeyPr type missmatched."));
        std::printf("im runnable from C\n");
        i = 10;
        //std::printf("after %d", i);
    }
    int i;
};
struct Base {
    Base() {    
        EventManager::subscribe<
        Base*, CollisionEvent,
        void (Base::* )(BaseEvent*)
        >(this,  &Base::run); 
    }
    virtual void run(BaseEvent* p) {
        std::printf("called on BASE\n");
    }
    virtual ~Base() { }
};
struct Derived:public Base {
    Derived(): Base() { }
    virtual void run(BaseEvent* p) {
        std::printf("called on DERIVED\n");
    }
    virtual ~Derived() { }
};

BOOST_AUTO_TEST_CASE(event_manager_test) {
    // signature subscribe<Event, Object, Call>(1, 2, 3);
    /* test:
    - fire !
    - clear
    - subscribe !
    - call_subs !
    - unsubscribe !
    - subscribe FROM BASE dispatch ON DERIVED

    */
    std::cout << "\033[32m*** event_manager_test\033[0m\n";
    int i = 7;
    KeyPressedEvent<int>* event = new KeyPressedEvent<int>(i);
    CollisionEvent* cevent = new CollisionEvent();
    B* b = new B();
    C* c = new C();
    // void (B::*)(KeyPressedEvent<int>*)
    // &B::runnable
    EventManager::fire(new KeyPressedEvent<int>(i));
    EventManager::fire(new CollisionEvent());
    EventManager::subscribe<
        B*, KeyPressedEvent<int>,
        void (B::* )(BaseEvent*)
        >(b,  &B::runnable);
    
    EventManager::subscribe<
        C*, CollisionEvent, 
        void (C::* )(BaseEvent*)
        >(c, &C::executable);

    EventManager::call_subs();
    std::printf("after: %d\n", b->i);
    std::printf("after %d\n", c->i);
    std::printf("calling subs, no event dispatched\n");
    EventManager::call_subs();
    std::printf("calling subs, two events dispatched\n");
    EventManager::fire(new KeyPressedEvent<int>(i));
    EventManager::fire(new CollisionEvent());
    EventManager::call_subs();
    std::printf("calling subs, two events dispatched, one UNsubscribed\n");
    EventManager::fire(new KeyPressedEvent<int>(i));
    EventManager::fire(new CollisionEvent());
    EventManager::unsubscribe<B, KeyPressedEvent<int> >();
    EventManager::call_subs();
    std::printf("dispatching ONE event, subscribed on BASE, called on DERIVED\n");
    EventManager::fire(new CollisionEvent());
    auto der = new Derived();
    EventManager::call_subs();
    delete der;
}