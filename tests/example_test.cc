#define BOOST_TEST_MODULE example_test
#include "src/Game.hpp"
#include <boost/test/included/unit_test.hpp>
#include <type_traits>


BOOST_AUTO_TEST_CASE(first_test) {
    CGame* game = CGame::Instance();
    BOOST_CHECK(typeid(CGame).name() == typeid(*game).name());
}