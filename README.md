# BattleTanks aka BattleCity
## This is my implementation of BattleCity game with C++ using SFML. Project created for purely educational purposes. (project under development)
## Requirements
* GCC 8+ (C++ 17)
* CMake 3.7+
* SFML 2.8+
* Boost.Test 1.67+ (optional)
## Compilation
Assuming SFML is installed:
```
// From project root
cd build
cmake ..
make
```
## Compiling Tests
Tests require Boost installed. Also, compile them AFTER compiled project. The build of tests is far from optimal.
```
// From project root
cd tests
make
```
